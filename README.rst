NetSD: Remote Access to Integrated SD Cards of Embedded Devices
===============================================================

This repository is in the process of getting filled.

This repository contains the source code and documentation (hardware and
software) regarding a `publication <https://arxiv.org/pdf/2109.15322>`__
which will be published in the context of `1st International Workshop on
Testing Distributed Internet of Things Systems (TDIS)
<https://tdis21.diselab.berlin/>`__, co-located with the `9th IEEE
International Conference on Cloud Engineering (IC2E 2021)
<https://conferences.computer.org/IC2E/2021/>`__.

If you use this work in your work, please cite us::

  @inproceedings{schroeter2021netsd,
    title={NetSD: Remote Access to Integrated SD Cards of Embedded Devices},
    author={Valentin Schröter and Arne Boockmeyer and Lukas Pirl},
    booktitle = {2021 IEEE International Conference on Cloud Engineering (IC2E) Workshops},
    organization={IEEE},
    year={2021},
    pages={To appear},
    pdf={https://arxiv.org/pdf/2109.15322}
  }
