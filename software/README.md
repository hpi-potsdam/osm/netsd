
## Requirements

To compile the software, you need [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/).

Before you use IDF, you need to setup environment variables for it:

```
. /path/to/esp-if/export.sh
```

On the computer you want to use for NBD access, the NBD kernel module needs to be loaded:

```
sudo modprobe nbd
```

## Compiling
Now you can use the following commands to build and flash the project:

```
idf.py build
idf.py flash
```

Before you can use the NDB access, you need to set the correct wifi credentials in `main/config.h` and re-build/flash the project.

Once the software is flashed you can use the serial monitor to view program output, including the IP address of the NBD server.

```
idf.py monitor
```

You exit the serial monitor with `CTRL`+`5`.

## NBD Usage
For the following example, we assume we got the IP address `192.168.178.24` from serial monitor.

### Connect

```
sudo nbd-client 192.168.178.24 /dev/nbd0
```

`/dev/nbd0` is now a block device that can be used like a local one.

### Disconnect

```
sudo nbd-client 192.168.178.24 -d /dev/nbd0
```
